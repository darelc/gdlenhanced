
#include "StdAfx.h"
#include "PhatSDK.h"
#include "InferredCellData.h"

void CInferredCellData::Init()
{
#ifndef PUBLIC_BUILD
	SERVER_INFO << "Loading inferred cell data...";
#endif

	_data.Destroy();

	LoadCacheData(6, 0xcd57fd07, 0x697a2224, _data);

	std::filesystem::path dataPath("data/json");

	LoadJsonData(dataPath / "worldspawns.json", _jsonData);

	LoadJsonData(dataPath / "encounters.json", _encounters);

	// load individual spawn maps to override worldspawns.json
	std::mutex mapLock;
	PerformLoad("data/json/spawnMaps/", [&](std::filesystem::path path)
	{
		std::ifstream fs(path);

		json jsonData;
		bool parsed = false;
		CLandBlockExtendedData blockData;
		DWORD blockId = 0;
		try
		{
			fs >> jsonData;

			json::const_iterator end = jsonData.end();
			json::const_iterator key = jsonData.find("key");
			json::const_iterator val = jsonData.find("value");

			if (key != end)
			{
				blockId = *key;

				if (val != end)
					parsed = blockData.UnPackJson(*val);
			}
		}
		catch (std::exception &ex)
		{
			LOG_PRIVATE(Data, Error, "Failed to parse spawn map file %s\n", path.string().c_str());
		}

		fs.close();

		if (parsed)
		{
			std::scoped_lock lock(mapLock);
			_jsonData.landblocks.insert_or_assign(blockId, blockData);
		}

	});

#ifndef PUBLIC_BUILD
	SERVER_INFO << "Finished loading inferred cell data ("
		<< (DWORD)_data.landblocks.size() << "and" << (DWORD) _jsonData.landblocks.size()
		<< ") encounters" << _encounters.size() << "...";
#endif
}

CLandBlockExtendedData *CInferredCellData::GetLandBlockData(DWORD landblock)
{
	CLandBlockExtendedData *data = _jsonData.landblocks.lookup(landblock);

	if (!data)
	{
		data = _data.landblocks.lookup(landblock);
	}

	return data;
}
