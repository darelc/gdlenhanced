
#pragma once

#include "Packable.h"

class EffectArgument : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE();
	DECLARE_PACKABLE_JSON();

	int _type;

	union 
	{
		BYTE _raw[8];
		double dbl_value;
		int int_value;	
		struct quality_value_s
		{
			StatType statType;

			union
			{
				int statIndex;
				STypeInt intStat;
				STypeFloat floatStat;
			};
		} quality_value;
		struct range_value_s
		{
			float min;
			float max;
		} range_value;
	};

	// custom
	bool ResolveValue(class CACQualities *q, double *vars);
	void StoreValue(class CACQualities *q, double *vars);

	bool _isValid = false;

	/*
	bool _isFloat = false;
	int _resolvedValueInt = 0;
	double _resolvedValueFloat = 0.0;
	*/

	double _resolvedValue = 0.0;
};

class MutationEffect : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE();
	DECLARE_PACKABLE_JSON();

	EffectArgument _argQuality;
	int _effectType;
	EffectArgument _arg1;
	EffectArgument _arg2;
};

class MutationEffectList : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE();
	DECLARE_PACKABLE_JSON();

	double _probability;
	PackableListWithJson<MutationEffect> _effects;
};

class MutationOutcome : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE();
	DECLARE_PACKABLE_JSON();

	PackableListWithJson<MutationEffectList> _effectList;
};

class MutationChance : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE();
	DECLARE_PACKABLE_JSON();

	PackableListWithJson<double> _chances;
};

class Mutation : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE();
	DECLARE_PACKABLE_JSON();

	MutationChance _chance;
	PackableListWithJson<MutationOutcome> _outcomes;
};

class CMutationFilter : public PackObj, public PackableJson //, public DBObj
{
public:
	DECLARE_PACKABLE();
	DECLARE_PACKABLE_JSON();

	//DECLARE_DBOBJ(CMutationFilter)
	//DECLARE_LEGACY_PACK_MIGRATOR()

	void TryMutate(class CACQualities *q); // custom

	PackableListWithJson<Mutation> _mutations;
};