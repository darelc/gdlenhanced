#pragma once

#include "WeenieObject.h"
#include "house.h"
#include "Player.h"
#include "World.h"
#include "Server.h"
#include "Network.h"

class CHouseManager
{
public:
	CHouseManager();
	~CHouseManager();

	void Load();
	void Save();

	DWORD _currentHouseMaintenancePeriod = 0;
	DWORD _nextHouseMaintenancePeriod = 0;
	bool _freeHouseMaintenancePeriod = false;

	CHouseData *GetHouseData(DWORD houseId);
	void SaveHouseData(DWORD houseId);
	void SendHouseData(CPlayerWeenie *player, DWORD houseId);
	void RemoveHouseFromAccount(DWORD characterId);
	void ReleaseDeletedCharacterHousing();
	void ReleaseBannedCharacterHousing();
	void LoadHousingMap();
	std::tuple<int, std::list<DWORD>> GetAvailableHouses(HouseType houseType);

private:
	PackableHashTable<DWORD, CHouseData> _houseDataMap;

	std::mutex m_housingLock;


};
